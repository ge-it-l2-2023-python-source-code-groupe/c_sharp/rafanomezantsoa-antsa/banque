﻿using BanqueMada;

namespace TpBasique;
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Création du compte 1");
        Console.Write("Entrez le nom : ");
        string nom = Console.ReadLine() ?? "";
        Console.Write("Entrez le prénom : ");
        string prenom = Console.ReadLine() ?? "";
        Console.Write("Entrez le CIN : ");
        string cin = Console.ReadLine() ?? "";
        Console.Write("Entrez le numéro de téléphone : ");
        string tel = Console.ReadLine() ?? "";

        Client cl1 = new Client(cin, nom, prenom, tel);
        Banque ccl1 = new Banque(cl1);

        Console.WriteLine("Compte 1 : ");
        ccl1.AfficherCompte();

        Console.WriteLine("Créditez le compte : ");
        string credit = Console.ReadLine() ?? "";

        ccl1.Crediter(Convert.ToSingle(credit));
        Console.WriteLine($"Le solde du compte 1 est {ccl1.Solde}");

        Console.WriteLine("Création du compte 2");
        Console.Write("Entrez le nom : ");
        nom = Console.ReadLine() ?? "";
        Console.Write("Entrez le prénom : ");
        prenom = Console.ReadLine() ?? "";
        Console.Write("Entrez le CIN : ");
        cin = Console.ReadLine() ?? "";
        Console.Write("Entrez le numéro de téléphone : ");
        tel = Console.ReadLine() ?? "";

        Client cl2 = new Client(cin, nom, prenom, tel);
        Banque ccl2 = new Banque(cl2);

        Console.WriteLine("Compte 2 : ");
        ccl2.AfficherCompte();

        Console.WriteLine("Créditez le compte : ");
        credit = Console.ReadLine() ?? "";

        ccl2.Crediter(Convert.ToSingle(credit));
        Console.WriteLine($"Le solde du compte 1 est {ccl2.Solde}");

        Console.WriteLine(@"Créditer le compte 1 à partir du compte 2
        Entrez le montant à créditer : ");
        credit = Console.ReadLine() ?? "";
        ccl1.Crediter(Convert.ToSingle(credit), ccl2);
        Console.WriteLine("Opération effectué");

        ccl1.AfficherCompte();
        ccl2.AfficherCompte();
    }
}
