namespace BanqueMada;
class Banque {
    private static int num = 0;

    public float Solde { get; private set; }
    public int Code { get; private set; }
    public Client Cl { get; set; }

    public Banque(Client cli) {
        Solde = 0;
        Code = ++num;
        Cl = cli;
    }

    public void Crediter(float somme) {
        Solde += somme;
    }

    public void Crediter(float somme, Banque compte) {
        Solde += somme;
        compte.Debiter(somme);
    }

    public void Debiter(float somme) {
        Solde -= somme;
    }

    public void Debiter(float somme, Banque compte) {
        Solde -= somme;
        compte.Crediter(somme);
    }

    public void AfficherCompte() {
        Console.WriteLine($@"Numéro du compte : {Code} 
Solde du compte : {Solde} 
Propriétaire du compte :");
        Cl.Afficher();
    }
}